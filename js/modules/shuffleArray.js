import chunkArray   from './chunkArray.js';
export default function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    let arrayFinal = chunkArray(array, 2);
    return arrayFinal;
  }