import shuffleArray from './shuffleArray.js';

export default function initGeraDuplas() {
    const input       = document.querySelector('input');
    const btnRand     = document.querySelector('[data-randomize]');    
    const grupos      = document.querySelector('#grupos');
    let count         = 0;
    let participantes = [];
    let formulario    = document.querySelector('.form-adicionar');
    
    formulario.addEventListener('submit', adicionar);  
    btnRand.addEventListener('click', geraDuplas);

    function getValorInput() {
        const nomeParticipante = input.value;
        return nomeParticipante;
    }

    function criaBotaoDelete(id) {
        let btnDelete               = document.createElement('BUTTON');
        btnDelete.innerText         = 'deletar';
        btnDelete.dataset.delete    = id;  
        btnDelete.addEventListener('click', deletar);
        return btnDelete;
    }

    function criaLi() {
        let itemLista = document.createElement('LI');
        return itemLista;
    }  

    function criaSpan() {
        return document.createElement('span');
    }   

    function setaDataNoSpanETexto(span, nome, count) {
        span.dataset.id = count;
        span.innerText = nome;    
        return span;
    } 

    function insereHTMLNaLi(item) {
        let aux = criaLi();
        aux.appendChild(item);
        return aux;
    }  

    function appendItem(pai, filho) {
        pai.appendChild(filho);
    } 

    function resetaValorInput(input) {
        input.value = '';
    }  

    function criaLista(itemLista) {
        const lista = document.querySelector('#listaParticipantes');
        lista.appendChild(itemLista);
        return lista;
    }
    
    function adicionar(e) {
        e.preventDefault();
        
        const nome        = getValorInput();
        const btnDelete   = criaBotaoDelete(count);
        const span        = criaSpan();
        const itemLista   = insereHTMLNaLi(span);
        const lista       = criaLista(itemLista);
    
        setaDataNoSpanETexto(span, nome, count);
        
        appendItem(itemLista, btnDelete);
        appendItem(lista, itemLista);
        
        adicionaNomeNaArray(participantes, nome);
        resetaValorInput(input);
        count++;
    }
    
    function adicionaNomeNaArray(array, nome) {
        array.push(nome);
    }
    
    function deletaDoDOM(e) {
        e.remove();
    }
    
    function deletar() {
        let idDeletado      = this.getAttribute('data-delete');
        let valueDeletado   = document.querySelector(`[data-id="${idDeletado}"]`);
        valueDeletado       = valueDeletado.innerText;
        participantes       = participantes.filter(item => item !== valueDeletado);
        deletaDoDOM(this.parentElement);    
    }
    
    function geraDuplas() {
        let arrayMisturado = shuffleArray(participantes);
        arrayMisturado.forEach((dupla, index) => {
            let li = criaLi();
            dupla.forEach((jogador) => {
                let span = criaSpan();
                setaDataNoSpanETexto(span, jogador, index);
                appendItem(li, span);
            });
            appendItem(grupos, li);
        });
    }
}