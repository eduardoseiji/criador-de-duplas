<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Rand de duplas do torneio de sinuca Webart</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <main id="main">
      <div class="container">
        <div class="row">
        <div class="col-12 d-flex justify-content-center">
          <h1>Gerador de duplas</h1>
        </div>
          <div class="col-12 d-flex block-section align-items-center">
            <h2>Insira um nome</h2>
            <form class="form-adicionar d-flex" action="#" method="post">
              <input type="text" name="" value="">
              <button type="submit" name="button">Adicionar</button>
            </form>
          </div>
          <div class="col-12 block-section">
            <h2>Lista de participantes</h2>
            <ul id="listaParticipantes" class="listaParticipantes">
            </ul>
            <button type="button" name="button" data-randomize>Gerar duplas</button>
            <ul id="grupos" class="grupos"></ul>
          </div>
        </div>
      </div>
    </main>

    <script src="js/main.js" charset="utf-8" type="module"></script>
  </body>
</html>
